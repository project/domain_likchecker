<?php

/**
 * Implements hook_user_view().
 */
function domain_linkchecker_user_view($account, $view_mode) {

  $account->content['domain_linkchecker'] = array(
    '#type' => 'user_profile_category',
    '#weight' => 40,
    '#title' => t('Broken links on your domains:'),
  );

  $account->content['domain_linkchecker']['broken_links'] = array(
    '#title' => '',
    '#type' => 'user_profile_item',
    '#markup' => domain_linkchecker_domain_report($account),
  );
}

/**
 * Menu callback for domain specific reporting.
 */
function domain_linkchecker_domain_report($account) {
    
  $ignore_response_codes = preg_split('/(\r\n?|\n)/', variable_get('linkchecker_ignore_response_codes', "200\n302\n304\n401\n403")); 
  $output = '';
  $result = db_query("SELECT d.domain_id, d.subdomain FROM {domain} d JOIN {domain_editor} de ON d.domain_id = de.domain_id WHERE de.uid = :uid", array(':uid' => $account->uid));

  foreach ($result as $domain) {
    //$result = db_query("SELECT lc.* FROM {linkchecker_links} lc JOIN {linkchecker_node} ln ON lc.lid = ln.lid JOIN {node} n ON ln.nid=n.nid JOIN {domain_access} da ON n.nid = da.nid FROM domain_editor WHERE domain_id=%d", $domain->domain_id);// Build query for broken links in nodes of the current user.
    $subquery2 = db_select('node', 'n');
    $subquery2->innerJoin('domain_access', 'da', 'da.nid = n.nid');
    $subquery2->innerJoin('linkchecker_node', 'ln', 'ln.nid = n.nid');
    $subquery2->innerJoin('linkchecker_link', 'll', 'll.lid = ln.lid');
    $subquery2->condition('ll.last_checked', 0, '<>');
    $subquery2->condition('ll.status', 1);
    $subquery2->condition('ll.code', $ignore_response_codes, 'NOT IN');
    $subquery2->condition('da.gid', $domain->domain_id);
    $subquery2->distinct();
    $subquery2->fields('ll' , array('lid'));

    $subquery1 = db_select($subquery2, 'q1')->fields('q1', array('lid'));

    // Build pager query.
    $query = db_select('linkchecker_link', 'll')->extend('PagerDefault')->extend('TableSort');
    $query->innerJoin($subquery1, 'q2', 'q2.lid = ll.lid');
    $query->fields('ll');
    $query->condition('ll.last_checked', 0, '<>');
    $query->condition('ll.status', 1);
    $query->condition('ll.code', $ignore_response_codes, 'NOT IN');

    $output .= '<h4>' . $domain->subdomain . '</h4>';
    $output .= drupal_render(_domain_linkchecker_report_page($query, $account, $domain));

  }
  return $output;   
}

function _domain_linkchecker_report_page($query, $account = NULL, $domain) {

  if (user_access('administer linkchecker')) {
    $links_unchecked = db_query('SELECT COUNT(1) FROM {linkchecker_link} WHERE last_checked = :last_checked', array(':last_checked' => 0))->fetchField();
    if ($links_unchecked > 0) {
      $links_all = db_query('SELECT COUNT(1) FROM {linkchecker_link}')->fetchField();
      drupal_set_message(format_plural($links_unchecked,
        'There is 1 unchecked link of about @links_all links in the database. Please be patient until all links have been checked via cron.',
        'There are @count unchecked links of about @links_all links in the database. Please be patient until all links have been checked via cron.',
        array('@links_all' => $links_all)), 'warning');
    }
  }  

  $header = array(
    array('data' => t('URL'), 'field' => 'url', 'sort' => 'desc'),
    array('data' => t('Response'), 'field' => 'code', 'sort' => 'desc'),
    array('data' => t('Error'), 'field' => 'error'),
    array('data' => t('Operations')),
  );

  $result = $query
    ->limit(10)
    ->orderByHeader($header)
    ->execute();

  // Evaluate permission once for performance reasons.
  $access_edit_link_settings = user_access('edit link settings');
  $access_administer_blocks = user_access('administer blocks');
  $access_administer_redirects = user_access('administer redirects');

  $rows = array();
  foreach ($result as $link) {
    $links = array();

    // Show links to link settings.
    if ($access_edit_link_settings) {
      $links[] = l(t('Edit link settings'), 'linkchecker/' . $link->lid . '/edit', array('query' => drupal_get_destination()));
    }

    // Show link to nodes accessible by domain user having this broken link.
    $nodes = db_query('SELECT ln.nid
        FROM {linkchecker_node} ln
        INNER JOIN {node} n ON n.nid = ln.nid
        INNER JOIN {domain_access} da ON da.nid = n.nid
        WHERE ln.lid = :lid AND da.gid = :gid', array(':lid' => $link->lid, ':gid' => $domain->domain_id));

    foreach ($nodes as $node) {
      $links[] = l(t('Edit node @node', array('@node' => $node->nid)), 'node/' . $node->nid . '/edit', array('query' => drupal_get_destination()));
    }

    // Show link to blocks having this broken link.
    if ($access_administer_blocks) {
      $blocks_custom = db_query('SELECT bid FROM {linkchecker_block_custom} WHERE lid = :lid', array(':lid' => $link->lid));
      foreach ($blocks_custom as $block_custom) {
        $links[] = l(t('Edit block @block', array('@block' => $block_custom->bid)), 'admin/build/block/configure/block/' . $block_custom->bid, array('query' => drupal_get_destination()));
      }
    }

    // Show link to redirect this broken internal link.
    if (module_exists('path_redirect') && $access_administer_redirects && _linkchecker_is_internal_url($link)) {
      $links[] = l(t('Create redirection'), 'admin/build/path-redirect/add', array('query' => array('src' => $link->internal)));
    }

    // Create table data for output. Use inline styles to prevent extra CSS file.
    $rows[] = array('data' =>
      array(
        l(_filter_url_trim($link->url, 40), $link->url),
        $link->code,
        check_plain($link->error),
        theme('item_list', array('items' => $links))
      ),
    );
  }

  if (empty($rows)) {
    $rows[] = array(array('data' => t('No broken links have been found.'), 'colspan' => count($header)));
  }

  $build['linkchecker_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
  );
  $build['linkchecker_pager'] = array('#theme' => 'pager');

  return $build;
}

